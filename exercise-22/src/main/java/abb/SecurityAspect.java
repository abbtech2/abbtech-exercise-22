package abb;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class SecurityAspect {

    @Pointcut("@annotation(Secured)")
    public void securedMethod(){

    }

    @Before("securedMethod() && @annotation(secured)")
    public void checkSecurity(JoinPoint joinPoint, Secured secured) {
        Object[] args = joinPoint.getArgs();
        if (args.length > 0 && args[0] instanceof Role) {
            Role role = (Role) args[0];
            for (String allowedRole : secured.roles()) {
                if (role.name().equals(allowedRole)) {
                    System.out.println("Authorized");
                    return;
                }
            }
        }
        System.out.println("Not Authorized");
    }

}

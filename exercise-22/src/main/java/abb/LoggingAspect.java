package abb;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class LoggingAspect {

    @Pointcut("@annotation(Loggable)")
    public void loggableMethod() {}

    @Before("loggableMethod() && @annotation(loggable)")
    public void logBefore(JoinPoint joinPoint, Loggable loggable) {
        System.out.println("Priority: " + loggable.priority());
    }
}

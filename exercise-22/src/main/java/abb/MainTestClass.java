package abb;

public class MainTestClass {

    @Secured(roles = {"ADMIN", "USER"})
    public void securedMethod(Role role) {
        System.out.println("Inside securedMethod");
    }

    @Loggable(priority = Priority.HIGH)
    public void unsecuredMethod() {
        System.out.println("Inside loggable");
    }
}

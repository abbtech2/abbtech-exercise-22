package abb;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        MainTestClass testClass = new MainTestClass();
        testClass.securedMethod(Role.ADMIN);
        testClass.securedMethod(Role.DEV);
        testClass.unsecuredMethod();
    }
}